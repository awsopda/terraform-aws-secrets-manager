output "secret" {
  description = "The password returned to be used."
  value       = jsondecode(data.aws_secretsmanager_secret_version.this.secret_string)
  sensitive   = true
}
